#include "RD53BPlugins/RD53BControllable.h"
#include <RD53BEmulator/ExternalTriggerScan.h>
#include <string>

using namespace std;
using namespace RD53B;

RD53BControllable::RD53BControllable():daq::rc::Controllable(){
  //The constructor should be a quick method when the object is created 
  //This happens when the application is initialized
  cout << "RD53BControllable::Create Controllable" << endl;
  m_regHandler = 0;
  m_handler = 0;
  //After creating the object it should be possible 
  //To configure parameters like
  //- bus path
  //- interface
  //- mapping
}

RD53BControllable::~RD53BControllable(){
  //Nothing should happen in the destructor
  //To be consistent with the FSM, whatever is initialized in configure
  //should be deleted in unconfigure
  //we put here a protection in case the object is deleted without unconfiguring
  cout << "RD53BControllable::Delete Controllable" << endl;
  if(m_handler) delete m_handler;
  if(m_regHandler) delete m_regHandler;
}

void RD53BControllable::configure(const daq::rc::TransitionCmd&){
  //FSM transition from INITIAL to CONFIGURED
  cout << "RD53BControllable::Configure" << endl;
 
  //Setup first: initialize the classes that we are going to use 
  cout << "RD53BControllable::Create External Trigger Scan" << endl;
  
  m_handler = new ExternalTriggerScan();
  m_handler->SetVerbose(true);
  cout << "Loading the bus for the handler" << endl;
  m_handler->SetBusPath("/tmp/bus/");
  cout << "Loading the interface for the handler" << endl;
  m_handler->SetInterface("lo");
  cout << "Loading the configuration card for the handler"<< endl;
  //m_handler->SetMapping("connectivityFiles/connectivity-TDAQ.json",true);

  
  m_handler->AddMapping("CERNQ20_Chip2","RD53BConfigs/CERNQ20_FE2_merge4to1.json",0x1000000000048000,0x1000000000000000);
  m_handler->AddFE("CERNQ20_Chip2");
  cout << "RD53BControllable::Create Loading Felix Register Handler" << endl;
  // We need to make this configurable and loadable. 
  m_regHandler = new FLXRegisterHandler("lo", "/tmp/bus/", 0x1000000812008000, false); 

  cout << "RD53BControllable::configure END" << endl;
}

void RD53BControllable::connect(const daq::rc::TransitionCmd&){
  //FSM transition from CONFIGURED to CONNECTED
  cout << "RD53BControllable::Connect" << endl;
  
  //Connect to the front-ends
  m_handler->Connect();
  //Connect to felix-register
  m_regHandler->Connect();
 
  cout << "RD53BControllable::Connect END" << endl;
}

void RD53BControllable::prepareForRun(const daq::rc::TransitionCmd&){

  cout << "RD53BControllable::prepareForRun" << endl;

  //Actually configure the front-ends
  m_handler->Config();


  for(auto fe : m_handler->GetFEs()){ 
 
    cout << "DigitalScan: Configure digital injection" << endl;
    fe->GetConfig()->SetField(Configuration::CalMode,1); // Digital
    ///fe->GetConfig()->SetField(Configuration::CalAnaMode,0); // Analog
    fe->GetConfig()->SetField(Configuration::Latency,60);
    fe->GetConfig()->SetField(Configuration::PIX_BCAST_EN,0);
    fe->GetConfig()->SetField(Configuration::PIX_AUTO_ROW,0);
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,2000);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,100);
    fe->GetConfig()->SetField(Configuration::InjVcalRange,0);
    
    fe->WriteGlobal();
    m_handler->Send(fe);

    m_handler->ConfigurePixels(fe);

    m_handler->GetMasker()->SetRange(0,0,400,384);
    m_handler->GetMasker()->SetShape(400,384);
    m_handler->GetMasker()->SetFrequency(1);
    m_handler->GetMasker()->Build();
    m_handler->GetMasker()->GetStep(0);
    m_handler->ConfigMask();
      
  }






  

  
  //Actually configure the TTC emulator
  std::map<std::string, uint64_t> regs;
  regs["TTC_EMU_SEL"]=1;
  regs["TTC_EMU_ENA"]=1;
  regs["TTC_EMU_BCR_PERIOD"]=3000;
  regs["TTC_EMU_ECR_PERIOD"]=0;
  regs["TTC_EMU_L1A_PERIOD"]=0;
  regs["TTC_DEC_CTRL_TT_BCH_EN"]=0x00;
  m_regHandler->sendRegs(regs);

  // std::vector<std::string> regs_names;
  // regs_names.push_back("TTC_EMU_SEL");
  // regs_names.push_back("TTC_EMU_ENA");
  // regs_names.push_back("TTC_EMU_BCR_PERIOD");
  // regs_names.push_back("TTC_EMU_ECR_PERIOD");
  // regs_names.push_back("TTC_EMU_L1A_PERIOD");
  // regs_names.push_back("TTC_DEC_CTRL_TT_BCH_EN");


  
  // m_regHandler->readRegs(reg_names);
  
  //Prepare for run 
  m_handler->PreRun();

  //call the run thread
  startExecution();
}

void RD53BControllable::run(){
  cout << "RD53BControllable::Run" << endl;

  m_regHandler->sendECR();
  m_regHandler->sendBCR();

  this_thread::sleep_for(chrono::seconds(3 ));
  std::map<std::string, uint64_t> regs;


  // regs["TTC_EMU_L1A_PERIOD"]=800; // This should be 1MHz trigger 
  // regs["TTC_EMU_ECR_PERIOD"]=4000; // This should be 1MHz trigger 
  // regs["TTC_EMU_L1A_PERIOD"]=40; // This should be 1MHz trigger 
  regs["TTC_EMU_ECR_PERIOD"]=40000; // This should be 1MHz trigger 
  regs["TTC_EMU_L1A_PERIOD"]=120; // This should be 1MHz trigger 



  m_regHandler->sendRegs(regs);
  


  m_handler->PrepareTrigger(61, 1, true, true, true, true,1);   
  //Start the trigger loop
  while(true){
    m_handler->Trigger();
  }


  // VD experimental
  // m_regHandler->sendBCR();
  ///m_regHandler->sendECR();
  // std::cout << " VALERIO:: DONE WITH RESET " << std::endl;
  // regs["TTC_EMU_L1A_PERIOD"]=40; 
  // m_regHandler->sendRegs(regs);

  // for (unsigned int t=0; t<150; t++) {
  //   //this_thread::sleep_for(chrono::microseconds( 1 ));
  //   m_regHandler->sendTrigger();
  //   std::cout << " Sent trigger: " << t << std::endl;
  //}
  
  // m_regHandler->sendECR();

  // 

  // regs["TTC_EMU_ECR_PERIOD"]=40000;
  // regs["TTC_EMU_L1A_PERIOD"]=40; 
  // m_regHandler->sendRegs(regs);

  // this_thread::sleep_for(chrono::microseconds( 1000 ));

  // regs["TTC_EMU_L1A_PERIOD"]=0; 
  // m_regHandler->sendRegs(regs);


  // for (unsigned int t=0; t<100; t++) {
  //   //this_thread::sleep_for(chrono::microseconds( 1 ));
  //   m_regHandler->sendTrigger();
  //   std::cout << " Sent trigger: " << t << std::endl;
  // }

  // m_regHandler->sendECR();

  // this_thread::sleep_for(chrono::microseconds( 1000 ));

  // for (unsigned int t=0; t<100; t++) {
  //   //this_thread::sleep_for(chrono::microseconds( 1 ));
  //   m_regHandler->sendTrigger();
  //   std::cout << " Sent trigger: " << t << std::endl;
  // }





  waitForCondition(CANCEL_REQUESTED);

  regs["TTC_EMU_L1A_PERIOD"]=0; 
  m_regHandler->sendRegs(regs);
  
  cout << "RD53BControllable::Run END" << endl;
}


void RD53BControllable::stopDC(const daq::rc::TransitionCmd&){
  cout << "RD53BControllable::stopDC" << endl;
  stopExecution();
  cout << "RD53BControllable::stopDC END" << endl;
}

void RD53BControllable::disconnect(const daq::rc::TransitionCmd&){
  cout << "RD53BControllable::Disconnect" << endl;
  m_handler->Disconnect();  

  //Enabeling TTC Emulator with given emulator settings
  std::map<std::string, uint64_t> regs;
  regs["TTC_EMU_SEL"]=0;
  regs["TTC_EMU_ENA"]=0;
  regs["TTC_EMU_BCR_PERIOD"]=3563;
  regs["TTC_EMU_ECR_PERIOD"]=0;
  regs["TTC_EMU_L1A_PERIOD"]=0;
  regs["TTC_DEC_CTRL_TT_BCH_EN"]=0x00;
  m_regHandler->sendRegs(regs);
  
  m_regHandler->Disconnect();

  cout << "RD53BControllable::Disconnect END" << endl;
}

void RD53BControllable::unconfigure(const daq::rc::TransitionCmd&){
  std::cout<<"RD53BControllable::unconfigure Disabling TTC Emulator" <<std::endl;

  //Delete the handler
  delete m_handler;
  m_handler = 0;

  //Delete the felix-register
  delete m_regHandler;
  m_regHandler = 0;
  
  cout << "RD53BControllable::unconfigure END" << endl;
}

void RD53BControllable::cleanup(){

  cout << "RD53BControllable::Cleanup eventually" << endl;

}

