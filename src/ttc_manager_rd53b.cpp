#include <ipc/core.h>
#include <ipc/partition.h>
#include <config/Configuration.h>
#include <dal/ResourceBase.h>
#include <dal/Partition.h>
#include <dal/Segment.h>
#include <dal/RunControlApplication.h>
#include "RunControl/Common/CmdLineParser.h"
#include <RunControl/ItemCtrl/ItemCtrl.h>
#include "RD53BPlugins/RD53BControllable.h"
#include <tmgr/tmresult.h>



void null_deleter(RD53BControllable *) {}

int main(int argc, char **argv){
  
  std::cout << "#######################################" << std::endl
       << "# Welcome to ttc_manager_rd53b        #" << std::endl
       << "#######################################" << std::endl;

  // Print the program arguments
  for (int i=0;i<argc;i++) std::cout << argv[i] << " ";
  std::cout << std::endl;

  std::cout << "Initialize the IPC" << std::endl;
  try {
    IPCCore::init(argc, argv);
  }catch(daq::ipc::CannotInitialize& e) {
    ERS_DEBUG(0, "Can not initialize IPC: " << e);
    return daq::tmgr::TmUnresolved;
  }catch(daq::ipc::AlreadyInitialized& e) {
    ERS_DEBUG(0, "IPC already initialized: " << e);
  }

  std::cout << "Create the cmdline parser" << std::endl;
  daq::rc::CmdLineParser parser(argc,argv,false);

  std::cout << "Partition: " << parser.partitionName() << std::endl;
  std::cout << "Application: " << parser.applicationName() << std::endl;
  std::cout << "Segment: " << parser.segmentName() << std::endl;
  std::cout << "Database: " << parser.database() << std::endl;
  
  /*
  IPCPartition ipc_partition(partition);
  if(!ipc_partition.isValid()){
    std::cout << "Invalid IPC Partition: " << partition << std::endl;
    return 1;
  }
  
  std::cout << "Open config db: " << database << std::endl;
  Configuration db(database);
  std::cout << "Get dal partition: " << partition << std::endl;
  const daq::core::Partition * dal_partition = db.get<daq::core::Partition>(partition);
  //load all the applications in the database
  dal_partition->get_all_applications();
  std::cout << "Get application: " << name << std::endl;
  const daq::core::RunControlApplication * app = db.get<daq::core::RunControlApplication>(name);
  const daq::core::Segment * seg = app->get_segment();
  if (segment=="") segment = seg->UID();
  std::cout << "Get segment: " << segment << std::endl;
  std::cout << "Get resources" << std::endl;
  vector<string> vmods;
  for(auto resource : seg->get_Resources()){
    if(resource->disabled(*dal_partition)) continue;
    vmods.push_back(resource->UID());
  }
  */
  
  std::cout << "Create the controllable" << std::endl;
  //  RD53BControllable * dd= new RD53BControllable();
  //std::shared_ptr<RD53BControllable> controllable=std::make_shared<RD53BControllable>(&dd,&null_deleter);
  std::shared_ptr<RD53BControllable> controllable=std::make_shared<RD53BControllable>();
  
  std::cout << "Create the item controller" << std::endl;
  daq::rc::ItemCtrl myItem(parser,controllable);

  std::cout << "Initilize the controller" << std::endl;
  myItem.init();

  std::cout << "Run the controller" << std::endl;
  myItem.run();

  std::cout << "Have a nice day" << std::endl;
  return 0;

}

