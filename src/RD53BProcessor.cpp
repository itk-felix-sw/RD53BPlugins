#include "RD53BPlugins/RD53BProcessor.h"
#include <chrono>

using namespace std;
using namespace RD53B;



RD53BProcessor::RD53BProcessor(){
  m_decoder = new Decoder();  
  m_TotalL1ID = 0;
  m_L1IDFollow = 0;
  m_HistNumDataPack = new swrod::detail::Histogram("ItkPix",
                     "/" + getName() + "/HistPerEvent", "Number of Data Packets",
						1, 2);
  m_HistMissed = new swrod::detail::Histogram("ItkPix",
                     "/" + getName() + "/HitsMissed", "Number of Missed Hits",
						65536, 65536);

  m_HistRow = new swrod::detail::Histogram("ItkPix",
                     "/" + getName() + "/HistRow", "Row of a Pixel Hit",
						400, 400);
  m_HistColumn = new swrod::detail::Histogram("ItkPix",
                     "/" + getName() + "/HistCol", "Column of a Pixel Hit",
						400, 400);
  m_HistToT = new swrod::detail::Histogram("ItkPix",
                     "/" + getName() + "/HistToT", "ToT of a Pixel Hit",
						16, 16);
  m_HistBCID = new swrod::detail::Histogram("ItkPix",
                     "/" + getName() + "/HistBCID", "BCID DataPacket",
						3570, 3570);

  m_HistL1ID = new swrod::detail::Histogram("ItkPix",
                     "/" + getName() + "/HistL1ID", "L1ID DataPacket",
						65536, 65536);

  m_HistDataSize = new swrod::detail::Histogram("ItkPix",
                     "/" + getName() + "/HistDataSize", "Data Size in Units of 32Bit",
						10, 10);

  
  

   
}

RD53BProcessor::~RD53BProcessor(){
  delete m_decoder;
  delete m_HistNumDataPack;
  delete m_HistRow;
  delete m_HistColumn;
  delete m_HistToT;
  delete m_HistL1ID;
  delete m_HistBCID;
  delete m_HistDataSize;
}

void RD53BProcessor::userCommand(const daq::rc::UserCmd & cmd){
  cout << "RD53BProcessor::userCommand" << endl;
}

void RD53BProcessor::linkDisabled(const swrod::InputLinkId & linkid){
  cout << "RD53BProcessor::linkDisabled " << linkid << endl;
}

void RD53BProcessor::linkEnabled(const swrod::InputLinkId & linkid){
  cout << "RD53BProcessor::linkEnabled " << linkid << endl;
}

void RD53BProcessor::processROBFragment(swrod::ROBFragment & fragment){
  //ERS_LOG("Decoding the ROBFragment");
  m_TotalL1ID++;
  //ERS_LOG("Fragment info: Total L1IDs "<<std::dec<< m_TotalL1ID <<" l1id: 0x"<<std::hex<<fragment.m_l1id<<std::dec<<" bcid: "<<fragment.m_bcid<<" TrigType: "<<fragment.m_trigger_type<<" Missed: "<<fragment.m_missed_packets<<" Corrupt: "<<fragment.m_corrupted_packets);  
    
  if(fragment.m_l1id !=(m_L1IDFollow+1) && fragment.m_l1id !=(m_L1IDFollow)){
    ERS_LOG("Missed Hits"<<fragment.m_l1id<<" "<<m_L1IDFollow);
    m_HistMissed->fill(fragment.m_l1id - m_L1IDFollow);
  }
  m_L1IDFollow=fragment.m_l1id;
  m_HistNumDataPack->fill(1);
  m_HistL1ID->fill(fragment.m_l1id  & 0xFFFF);
  m_HistBCID->fill(fragment.m_bcid);
  uint8_t *data = (uint8_t * ) (fragment.m_data[0].dataBegin()+2);
  uint8_t Tag8Bit = ((data[3] & 0x1F) << 3) | ( (data[2] >> 5 ) & 0x7 );
  //ERS_LOG("Data packet is: "<<std::hex<<int(data[0])<<" "<<int(data[1])<<" "<<int(data[2])<<" "<<int(data[3])<<std::dec);
  //ERS_LOG("Tag of 8 Bits: 0x"<<std::hex<<int(Tag8Bit)<<" Tag of 6 bits: 0x"<<int(Tag8Bit>>2)<<std::dec);


  
  for(uint32_t i=0;i<fragment.m_data.size();i++){
    m_decoder->Clear();
    //ERS_LOG("Fragment Content: "<<i<<"/"<<fragment.m_data.size()<<" size "<<fragment.m_data[i].dataSize());

    m_HistDataSize->fill(fragment.m_data[i].dataSize());
    
    //ERS_LOG("Dumping Fragment");
    for(unsigned ind=0;ind<fragment.m_data[i].dataSize();ind++){
      //ERS_LOG(std::hex<<int(fragment.m_data[i].dataBegin()[ind])<<" "<<std::dec);
    }
    //}
    //dataFragment datablock at 32 bit words and the first 2 32bits are FELIX stuff. So we start from 2nd and convert into 8bits by casting (Size is *4 to match the size)
    if(fragment.m_data[i].dataSize()>2){
      m_decoder->SetBytes((uint8_t*)((fragment.m_data[i].dataBegin())+2),(fragment.m_data[i].dataSize()-2)*4, true); 
      m_decoder->Decode(true); // Verbosity
      //ERS_LOG("Fragment Decoded with size of "<<m_decoder->GetFrames().size());
      //Temprory cout function to check if data-frames are comming
      for(auto frame: m_decoder->GetFrames()){

	DataFrame * dat=dynamic_cast<DataFrame*>(frame);
	//ERS_LOG("  Frame has N streams of "<<dat->GetNStreams()<<" ");
	for (unsigned iStream=0; iStream< dat->GetNStreams(); iStream++){ 
	  Stream * pStream = dat->GetStream(iStream);
	  for(unsigned iHit=0; iHit < pStream->GetNhits(); iHit++){
	    uint32_t HitMap = pStream->GetHitMap(iHit);
	    uint64_t ToTMap=0x7777777777777777; //If ToT value is not recovered from data-stream. Assume ToT=14
	    if (pStream->HasTot())
	      ToTMap = pStream->GetTotMap(iHit);
	    uint32_t ToTInd=0;
	    for (short iRow=0;iRow<2;iRow++){
	      for(short iCol=0;iCol<8;iCol++){
		if (( (HitMap >> (iRow*8+iCol)) & 0x1) == 0x1 ){
	      	
		  unsigned col = (pStream->GetQcol(iHit)-1)*8 + iCol;
		  unsigned row = (pStream->GetQrow(iHit))*2  + iRow;
		  uint8_t ToT = ((ToTMap>>(4*ToTInd))&0xF) ;
		  //ERS_LOG("col "<<col<<" row "<<row<<" ToT "<<std::hex<<int(ToT)<<std::dec);
		  m_HistRow->fill(row);
		  m_HistColumn->fill(col);
		  m_HistToT->fill(int(ToT));
		  ToTInd++;
		}
	      }
	    }
	  }
	}
      }
    }
  }
      

  for (unsigned int i=0;i<fragment.m_data.size();i++){ // until now, m_data always had a single entry only. 
    fragment.m_data[i].reset();
  }
}

extern "C" {
  std::tuple<uint32_t, uint32_t, uint16_t> decodeTriggerInfo(const uint8_t * data){
    // cout << "TriggerInfoExtractor called!" << endl;
    // cout << "VALERIO " << endl;
    uint8_t NewFrame=(data[3]>>7) & 0x1;
    uint8_t cID =(data[3]>>5) & 0x3;
    uint8_t Tag8Bit = ((data[3] & 0x1F) << 3) | ( (data[2] >> 5 ) & 0x7 );
    uint8_t Tag = Tag8Bit >> 2; //The first two bits of the tag reference to the Trigger identifier for L1A
     
    //ERS_LOG( std::hex<<"Data Packet: "<<int(NewFrame)<<" cID: "<<int(cID)<<" Tag 8Bit: "<<int(Tag8Bit)<<" Tag: "<<int(Tag)<<" 2Bit "<<int(Tag8Bit &0x3) <<" Data: "<<int(data[3])<<" "<<int(data[2]));
    // cout<<"Data Packet: ";
    // for (unsigned ind=0; ind<16; ind++)
    //   cout<<std::hex<<int(data[ind])<<" ";
    // cout);

    // std::tuple(L1ID,L1D BitMask, BCID)  
    return std::tuple(Tag,0x3F,0xFFFF);
    // Tag8Bit = GlobalHack;
    // GlobalHack++;
    // return std::tuple(Tag8Bit & 0xFF ,0xFF,0xFFFF);
  } 

  std::optional<bool> checkDataIntegrity(const uint8_t * data){
    cout << "testDataIntegrityChecker called!" << endl;
    return true;
  }

  swrod::CustomProcessor * createRD53BProcessor(const boost::property_tree::ptree & configuration)
  {
    cout << "createRD53BProcessor called!" << endl;
    return new RD53BProcessor();
  } 
}


/*

TriggerInfoExtractor called!
VALERIO 
Data Packet: 1 cID: 3 Tag 8Bit: 56 Tag: 15 2Bit 2 Data: ea c0
2023-Nov-09 12:04:54,698 LOG [swrod::DefaultL1AInputHandler::dataReceived(...) at /builds/atlas-tdaq-software/tdaq-cmake/tdaq/tdaq-10-00-00/swrod/src/core/DefaultL1AInputHandler.cpp:91] First L1ID = 0x0
2023-11-09 12:04:55.193 INFO  client felix_client.cpp:1414: Answer on register command
2023-11-09 12:04:55.699 INFO  client felix_client.cpp:1414: Answer on register command
2023-11-09 12:04:56.205 INFO  client felix_client.cpp:1414: Answer on register command

TriggerInfoExtractor called!
VALERIO 
Data Packet: 1 cID: 3 Tag 8Bit: 5b Tag: 16 2Bit 3 Data: eb 60
2023-Nov-09 12:04:56,215 LOG [swrod::GBTModeWorker<ReceiveTTC>::dataReceived(...) at /builds/atlas-tdaq-software/tdaq-cmake/tdaq/tdaq-10-00-00/swrod/src/core/GBTModeWorker.h:475] Link 0x1000000000000000 data error: L1ID bit mask = 0x3f L1ID received = 0x0 BCID received = 0xffff current ROB L1ID = 0x1 current ROB BCID = 0x25b last l1id = 0x0 expected l1id = 0x1 trigger counter = 1
testDataIntegrityChecker called!
2023-Nov-09 12:04:56,215 LOG [swrod::GBTModeWorker<ReceiveTTC>::checkPacketCRC(...) at /builds/atlas-tdaq-software/tdaq-cmake/tdaq/tdaq-10-00-00/swrod/src/core/GBTModeWorker.h:385] Link 0x1000000000000000 data error: CRC check yields ok
2023-Nov-09 12:04:56,215 LOG [swrod::GBTModeWorker<ReceiveTTC>::dataReceived(...) at /builds/atlas-tdaq-software/tdaq-cmake/tdaq/tdaq-10-00-00/swrod/src/core/GBTModeWorker.h:491] Link 0x1000000000000000 data error: duplicate packet will be dropped
2023-11-09 12:04:56.711 INFO  client felix_client.cpp:1414: Answer on register command
2023-11-09 12:04:57.217 INFO  client felix_client.cpp:1414: Answer on register command
2023-11-09 12:04:57.723 INFO  client felix_client.cpp:1414: Answer on register command

TriggerInfoExtractor called!
VALERIO 
Data Packet: 1 cID: 3 Tag 8Bit: 5f Tag: 17 2Bit 3 Data: eb e0
2023-Nov-09 12:04:57,725 LOG [swrod::GBTModeWorker<ReceiveTTC>::dataReceived(...) at /builds/atlas-tdaq-software/tdaq-cmake/tdaq/tdaq-10-00-00/swrod/src/core/GBTModeWorker.h:475] Link 0x1000000000000000 data error: L1ID bit mask = 0x3f L1ID received = 0x0 BCID received = 0xffff current ROB L1ID = 0x1 current ROB BCID = 0x25b last l1id = 0x0 expected l1id = 0x1 trigger counter = 1
testDataIntegrityChecker called!
2023-Nov-09 12:04:57,725 LOG [swrod::GBTModeWorker<ReceiveTTC>::checkPacketCRC(...) at /builds/atlas-tdaq-software/tdaq-cmake/tdaq/tdaq-10-00-00/swrod/src/core/GBTModeWorker.h:385] Link 0x1000000000000000 data error: CRC check yields ok
2023-Nov-09 12:04:57,725 LOG [swrod::GBTModeWorker<ReceiveTTC>::dataReceived(...) at /builds/atlas-tdaq-software/tdaq-cmake/tdaq/tdaq-10-00-00/swrod/src/core/GBTModeWorker.h:491] Link 0x1000000000000000 data error: duplicate packet will be dropped
2023-11-09 12:04:58.229 INFO  client felix_client.cpp:1414: Answer on register command
2023-11-09 12:04:58.735 INFO  client felix_client.cpp:1414: Answer on register command
2023-11-09 12:04:59.241 INFO  client felix_client.cpp:1414: Answer on register command

TriggerInfoExtractor called!
VALERIO 
Data Packet: 1 cID: 3 Tag 8Bit: 61 Tag: 18 2Bit 1 Data: ec 20
2023-Nov-09 12:04:59,243 LOG [swrod::GBTModeWorker<ReceiveTTC>::dataReceived(...) at /builds/atlas-tdaq-software/tdaq-cmake/tdaq/tdaq-10-00-00/swrod/src/core/GBTModeWorker.h:475] Link 0x1000000000000000 data error: L1ID bit mask = 0x3f L1ID received = 0x0 BCID received = 0xffff current ROB L1ID = 0x1 current ROB BCID = 0x25b last l1id = 0x0 expected l1id = 0x1 trigger counter = 1
testDataIntegrityChecker called!
2023-Nov-09 12:04:59,243 LOG [swrod::GBTModeWorker<ReceiveTTC>::checkPacketCRC(...) at /builds/atlas-tdaq-software/tdaq-cmake/tdaq/tdaq-10-00-00/swrod/src/core/GBTModeWorker.h:385] Link 0x1000000000000000 data error: CRC check yields ok
2023-Nov-09 12:04:59,243 LOG [swrod::GBTModeWorker<ReceiveTTC>::dataReceived(...) at /builds/atlas-tdaq-software/tdaq-cmake/tdaq/tdaq-10-00-00/swrod/src/core/GBTModeWorker.h:491] Link 0x1000000000000000 data error: duplicate packet will be dropped



*/
