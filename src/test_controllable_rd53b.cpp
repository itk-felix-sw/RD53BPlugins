#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RD53BPlugins/RD53BTestControllable.h"
#include "tmgr/tmresult.h"
#include "ipc/core.h"
#include <iostream>

int main(int argc, char **argv){
  
  std::cout << "#######################################" << std::endl
	    << "# Welcome to test_controllable_rd53b  #" << std::endl
	    << "#######################################" << std::endl;
  
  // Print the program arguments
  for (int i=0;i<argc;i++) std::cout << argv[i] << " ";
  std::cout << std::endl;
  
  std::cout << "Initialize the IPC" << std::endl;
  try {
    IPCCore::init(argc, argv);
  }catch(daq::ipc::CannotInitialize& e) {
    ERS_DEBUG(0, "Can not initialize IPC: " << e);
    return daq::tmgr::TmUnresolved;
  }catch(daq::ipc::AlreadyInitialized& e) {
    ERS_DEBUG(0, "IPC already initialized: " << e);
  }
  
  std::cout << "Create the cmdline parser" << std::endl;
  daq::rc::CmdLineParser parser(argc,argv,false);
  
  std::cout << "Partition: " << parser.partitionName() << std::endl;
  std::cout << "Application: " << parser.applicationName() << std::endl;
  std::cout << "Segment: " << parser.segmentName() << std::endl;
  std::cout << "Database: " << parser.database() << std::endl;
  
  
  std::cout << "Create the controllable" << std::endl;
  std::shared_ptr<RD53BTestControllable> controllable=std::make_shared<RD53BTestControllable>();
  
  std::cout << "Create the item controller" << std::endl;
  daq::rc::ItemCtrl myItem(parser,controllable);

  std::cout << "Initilize the controller" << std::endl;
  myItem.init();

  std::cout << "Run the controller" << std::endl;
  myItem.run();

  std::cout << "Have a nice day" << std::endl;
  return 0;

}

