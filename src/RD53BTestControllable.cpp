#include "RD53BPlugins/RD53BTestControllable.h"
#include "nlohmann/json.hpp"
//#include "RD53BEmulator/Handler.h"
#include <iostream>
#include <fstream>

using namespace std;

RD53BTestControllable::RD53BTestControllable():daq::rc::Controllable(){
  cout << "RD53BTestControllable::Create Controllable" << endl;
}

RD53BTestControllable::~RD53BTestControllable(){
  cout << "RD53BTestControllable::Delete Controllable" << endl;
}

void RD53BTestControllable::configure(const daq::rc::TransitionCmd&){
  cout << "RD53BTestControllable::Configure" << endl;

  /*
  RD53B::Handler * handler = new RD53B::Handler();
  handler->SetMapping("connectivityFiles/connectivity-TDAQ.json",true);  
  delete handler;
  */
  
  string itk_inst_path(getenv("ITK_INST_PATH"));
  string path(itk_inst_path+"/share/data");
  ifstream fr("connectivityFiles/connectivity-TDAQ.json");
  nlohmann::json config;
  fr >> config;
  cout << config << endl;
  
  cout << "RD53BTestControllable::configure END" << endl;
}

void RD53BTestControllable::connect(const daq::rc::TransitionCmd&){
  cout << "RD53BTestControllable::Connect" << endl;

  cout << "RD53BTestControllable::Connect END" << endl;
}

void RD53BTestControllable::prepareForRun(const daq::rc::TransitionCmd&){
  cout << "RD53BTestControllable::prepareForRun" << endl;
  
  //call the run thread
  startExecution();

  cout << "RD53BTestControllable::prepareForRun END" << endl;
}

void RD53BTestControllable::run(){
  cout << "RD53BTestControllable::Run" << endl;

  //Start the trigger loop
  waitForCondition(CANCEL_REQUESTED);

  cout << "RD53BTestControllable::Run END" << endl;
}


void RD53BTestControllable::stopDC(const daq::rc::TransitionCmd&){
  cout << "RD53BTestControllable::stopDC" << endl;
  stopExecution();
  cout << "RD53BTestControllable::stopDC END" << endl;
}

void RD53BTestControllable::disconnect(const daq::rc::TransitionCmd&){
  cout << "RD53BTestControllable::Disconnect" << endl;

  cout << "RD53BTestControllable::Disconnect END" << endl;
}

void RD53BTestControllable::unconfigure(const daq::rc::TransitionCmd&){
  cout << "RD53BTestControllable::unconfigure" << endl;

  cout << "RD53BTestControllable::unconfigure END" << endl;
}

void RD53BTestControllable::cleanup(){

  cout << "RD53BTestControllable::Cleanup eventually" << endl;

}

