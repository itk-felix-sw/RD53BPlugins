#!/usr/bin/env python

## Libraries required for optoBoard
from collections import OrderedDict
from optoboard_felix.driver.Optoboard import *
from optoboard_felix.driver.CommWrapper import CommWrapper
from optoboard_felix.driver.components import components
from optoboard_felix.driver.log import logger
from optoboard_felix.additional_driver.DB_access import configDbUnavailable, getConfigDataset


## Libraries required for PySerial
import os
import sys
import time
import signal
import PSUController
import PSURemoteController
import PSUConfig




## Variables that should be customasible for OptoBoard
optoboard_serial_1 = "2400021"
vtrx_v = "1.3"    # connected VTRx+ quad laser driver version

##Variable that are customasible for PySerial

ConfigFileOnly_Opto="PySerialComm/CRCard/OptoBoard.card"




####Setup PySerial

config=PSUConfig.PSUConfig(ConfigFileOnly_Opto)
psus=config.getPsus()
controller=PSUController.PSUController(psus)


#### Check internal Voltage Before Powering
for psu in psus:
    VoltageSet=controller.getSetVoltage(psu)
    CurrentLimit=controller.getCurrentLimit(psu)
    
    ##Compare them for the defaults in the config file 10% error
    if not (float(psus[psu]["Vmax"])*1.1 > VoltageSet and float(psus[psu]["Vmax"])*0.9 < VoltageSet ):
        print("Error PSU's set voltage doesn't agree with the one set on Opto")
        exit(1)

#### Turn on the Power
for psu in psus:
    controller.setOutput(psu,True)
    time.sleep(1) ##Sleep is need for the voltage values to update
    print("OptoBoard Power is on")
    VoltageOn=controller.getVoltage(psu)
    CurrentOn=controller.getCurrent(psu)

    if not (float(psus[psu]["Vmax"])*1.1 > VoltageOn 
            and float(psus[psu]["Vmax"])*0.9 < VoltageOn 
            and CurrentOn < float(psus[psu]["Imax"])):
            print("OptoBoard Power is differnet then expected, powering the optoBoard off")
            controller.setOutput(psu,True)
            exit(1)



#### Configure OptoBoard

# get Optoboard chip versions
components_opto_1 = OrderedDict(components[optoboard_serial_1])
# load configs
config_file_default = load_default_config(components_opto_1, vtrx_v)
# create wrapper to FELIX
communication_wrapper_1 = CommWrapper(flx_G=0, flx_d=0, lpgbt_master_addr=0x74, lpgbt_v=1, commToolName="ic-over-netio")
# initialise Optoboard objects
opto1 = Optoboard(config_file_default, optoboard_serial_1, vtrx_v, components_opto_1, communication_wrapper_1)
# Configure OptoBoard
opto1.configure()    # configure all available chips on the Optoboard



