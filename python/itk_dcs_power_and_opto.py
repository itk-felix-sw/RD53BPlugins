#!/usr/bin/env python

## RunControl
from rcpy import Controllable
import ers
from ipc import IPCPartition, getPartitions
from ispy import *

## Libraries required for optoBoard
from collections import OrderedDict
from optoboard_felix.driver.Optoboard import *
from optoboard_felix.driver.CommWrapper import CommWrapper
from optoboard_felix.driver.components import components
from optoboard_felix.driver.log import logger
from optoboard_felix.additional_driver.DB_access import configDbUnavailable, getConfigDataset

## Libraries required for PySerial
import os
import sys
import time
import signal
import PSUController
import PSURemoteController
import PSUConfig

## Variables that should be customasible for OptoBoard
optoboard_serial_1 = "2400021"
vtrx_v = "1.3"    # connected VTRx+ quad laser driver version

##Variable that are customasible for PySerial
#ConfigFileOnly_Opto="PySerialComm/CRCard/OptoBoard.card"
ConfigFileOnly_Opto="PySerialComm/CRCard/161R.card"

class itk_dcs_power_and_opto(Controllable):
    def __init__(self):
        super(itk_dcs_power_and_opto, self).__init__()
        ers.info("ITK-DCS starting")
        pass

    def configure(self,opts):    
        print("Creating itk_dcs_power_and_opto")
        self.psu_cntroller=None
        self.psu_config=None
        print("Create PSU config and controller")
        self.psu_config=PSUConfig.PSUConfig(os.getenv("ITK_INST_PATH")+"/../"+ConfigFileOnly_Opto)
        self.psu_cntroller=PSUController.PSUController(self.psu_config.getPsus())
        print("Check internal Voltage Before Powering and Making sure they are all powered off (Powering off)")
        for psu in self.psu_config.getPsus():
            VoltageSet=self.psu_cntroller.getSetVoltage(psu)
            CurrentLimit=self.psu_cntroller.getCurrentLimit(psu)
            self.psu_cntroller.setOutput(psu,False)
            ##Compare them for the defaults in the config file 10% error
            if not (float(self.psu_config.getPsus()[psu]["Vmax"])*1.1 > VoltageSet and float(self.psu_config.getPsus()[psu]["Vmax"])*0.9 < VoltageSet ):
                print("Error in",psu,"set voltage doesn't agree with the one set on defaults")
                return
        #### Turn on the Power for Opto
        for psu in self.psu_config.getPsus():
            if not "Opto" in psu: continue
            self.psu_cntroller.setOutput(psu,True)
            time.sleep(1) ##Sleep is need for the voltage values to update
            print(psu,"Power is on")
            VoltageOn=self.psu_cntroller.getVoltage(psu)
            CurrentOn=self.psu_cntroller.getCurrent(psu)
            if not (float(self.psu_config.getPsus()[psu]["Vmax"])*1.1 > VoltageOn 
                    and float(self.psu_config.getPsus()[psu]["Vmax"])*0.9 < VoltageOn 
                    and CurrentOn < float(self.psu_config.getPsus()[psu]["Imax"])):
                print(psu,"Power is differnet then expected, powering the optoBoard off")
                self.psu_cntroller.setOutput(psu,False)
            ers.info(psu+" power supplies up")                
            pass
        ### Make sure ITk Pix is powered off 
        print("Turning off the OptoBoard PSU's")
        for psu in self.psu_config.getPsus():
            if not "ItkPix" in psu: continue
            if not self.psu_cntroller.isEnabled(psu):
                self.psu_cntroller.setOutput(psu,False)
                time.sleep(1) ##Sleep is need for the voltage values to update
                print("OptoBoard Power is on")
                VoltageOn=self.psu_cntroller.getVoltage(psu)
                CurrentOn=self.psu_cntroller.getCurrent(psu)
                print(psu,"power is off V:",VoltageOn,"I:",CurrentOn)
            else:
                print(psu,"power is  already off V:",VoltageOn,"I:",CurrentOn)       
            pass


        #### Configure OptoBoard
        # get Optoboard chip versions
        components_opto_1 = OrderedDict(components[optoboard_serial_1])
        # load configs
        config_file_default = load_default_config(components_opto_1, vtrx_v)
        # create wrapper to FELIX
        communication_wrapper_1 = CommWrapper(flx_G=0, flx_d=0, lpgbt_master_addr=0x74, lpgbt_v=1, commToolName="itk-ic-over-netio-next")
        # initialise Optoboard objects
        opto1 = Optoboard(config_file_default, optoboard_serial_1, vtrx_v, components_opto_1, communication_wrapper_1)
        # Configure OptoBoard
        opto1.configure()    # configure all available chips on the Optoboard
        ers.info("Optoboard configured")
        
        for psu in self.psu_config.getPsus():
            if not "ItkPix" in psu: continue
            self.psu_cntroller.setOutput(psu,True)
            time.sleep(1) ##Sleep is need for the voltage values to update
            print("OptoBoard Power is on")
            VoltageOn=self.psu_cntroller.getVoltage(psu)
            CurrentOn=self.psu_cntroller.getCurrent(psu)
            if not (float(self.psu_config.getPsus()[psu]["Imax"])*1.1 > CurrentOn
                    and float(self.psu_config.getPsus()[psu]["Imax"])*0.9 < CurrentOn 
                    and VoltageOn < float(self.psu_config.getPsus()[psu]["Vmax"])):
                print(psu,"Current is differnet then expected, powering the ITkPix off")
                self.psu_cntroller.setOutput(psu,False) 
            ers.info(psu+" Power supplies up")               
            pass
        pass

    def prepareForRun(self, opts):
        pass
    def unconfigure(self, opts):
        ers.info("ITK-DCS starting")
        print("Creating itk_dcs_power_and_opto")
        self.psu_cntroller=None
        self.psu_config=None
        print("Create PSU config and controller")
        self.psu_config=PSUConfig.PSUConfig(os.getenv("ITK_INST_PATH")+"/../"+ConfigFileOnly_Opto)
        self.psu_cntroller=PSUController.PSUController(self.psu_config.getPsus())
        print("Turning off the OptoBoard PSU's")
        for psu in self.psu_config.getPsus():
            self.psu_cntroller.setOutput(psu,False)
            time.sleep(1) ##Sleep is need for the voltage values to update
            print("OptoBoard Power is on")
            VoltageOn=self.psu_cntroller.getVoltage(psu)
            CurrentOn=self.psu_cntroller.getCurrent(psu)
            print(psu,"power is off V:",VoltageOn,"I:",CurrentOn)
            pass
        ers.info("All Power supplies off")
        pass


