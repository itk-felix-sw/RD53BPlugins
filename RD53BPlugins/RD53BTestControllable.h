#ifndef RD53B_TEST_CONTROLLABLE_H
#define RD53B_TEST_CONTROLLABLE_H

#include "RunControl/Common/Controllable.h"
#include "DFThreads/DFThread.h"
#include <string>

class RD53BTestControllable : public DFThread, public daq::rc::Controllable{

 public:

  /**
   * Quick method when the object is created 
   * This happens when the application is initialized
   **/
  RD53BTestControllable();
  
  /**
   * delete module and register handlers if not deleted already
   **/
  virtual ~RD53BTestControllable();

  /**
   * FSM transition from INITIAL to CONFIGURED
   * Create the Hanlder and the FelixRegister
   * Load configuration into memory
   **/
  virtual void configure(const daq::rc::TransitionCmd&) override;

  /**
   * Connect to FELIX
   **/
  virtual void connect(const daq::rc::TransitionCmd&) override;

  /**
   * Configure the modules and the TTC
   * Create histograms
   * Start the DFThread
   **/
  virtual void prepareForRun(const daq::rc::TransitionCmd&) override;
  
  /**
   * Stop the DFThread
   **/
  virtual void stopDC(const daq::rc::TransitionCmd&) override;
  
  /**
   * Unconfigure the TTC
   * Disconnect from FELIX
   **/
  virtual void disconnect(const daq::rc::TransitionCmd&) override;
  
  /**
   * Delete the Handler and the FelixRegister
   **/
  virtual void unconfigure(const daq::rc::TransitionCmd&) override;

 protected:

  /**
   * The loop for the DFThread
   **/
  virtual void run();

  /**
   * The cleanup for the DFThread
   **/
  virtual void cleanup();
  
};

#endif
