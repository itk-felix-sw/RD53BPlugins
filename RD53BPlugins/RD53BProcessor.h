#ifndef RD53B_PROCESSOR
#define RD53B_PROCESSOR

#include "swrod/CustomProcessor.h"
#include "RD53BEmulator/Decoder.h"
#include "swrod/detail/Histogram.h"

class RD53BProcessor: public swrod::CustomProcessor{

  public:
 
  /**
   * Allocate the decoder
   **/
  RD53BProcessor();

  /**
   * Delete the decoder
   **/
  ~RD53BProcessor();

  /**
   * Nothing to do
   * @param cmd UserCmd to be interpreted
   **/
  void userCommand(const daq::rc::UserCmd & cmd);

  /**
   * Disable a link given the linkid
   * @param linkid the InputLinkId
   **/
  void linkDisabled(const swrod::InputLinkId & linkid);

  /**
   * Enable a link given the linkid
   * @param linkid the InputLinkId
   **/
  void linkEnabled(const swrod::InputLinkId & linkid);

  /**
   * Decode the fragment and build another fragment plus do histograms
   **/
  void processROBFragment(swrod::ROBFragment & fragment) override;

 private:
  
  RD53B::Decoder * m_decoder;
  uint64_t m_TotalL1ID;
  uint64_t m_L1IDFollow;
  swrod::detail::Histogram *m_HistNumDataPack;
  swrod::detail::Histogram *m_HistRow;
  swrod::detail::Histogram *m_HistColumn;
  swrod::detail::Histogram *m_HistToT;
  swrod::detail::Histogram *m_HistMissed;
  swrod::detail::Histogram *m_HistTag;
  swrod::detail::Histogram *m_HistL1ID;
  swrod::detail::Histogram *m_HistBCID; 
  swrod::detail::Histogram *m_HistDataSize;
};

extern "C" {
  std::tuple<uint32_t, uint32_t, uint16_t> decodeTriggerInfo(const uint8_t * data);
  std::optional<bool> checkDataIntegrity(const uint8_t * data);
  swrod::CustomProcessor * createRD53BProcessor(const boost::property_tree::ptree & configuration);

  }

#endif
